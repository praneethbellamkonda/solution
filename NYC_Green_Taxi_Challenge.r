
pkgs = c("ggplot2","scales","caret","lubridate","reshape2","dplyr","leaflet","sqldf","doParallel","cowplot","htmlwidgets","IRdisplay","corrplot","ModelMetrics")
inst = lapply(pkgs, library, character.only = TRUE,verbose = FALSE) 

cl <- makeCluster(4)
registerDoParallel(cl)
cat("Number of parallel workers initiated: ",getDoParWorkers())
#stopCluster(cl)

#Reading the data from the provided url.
Taxidata <- read.csv("https://s3.amazonaws.com/nyc-tlc/trip+data/green_tripdata_2015-09.csv")

#Print the dataset dimenstions
cat("Number of rows present in the dataset:",nrow(Taxidata),"rows\n")
cat("Number of columns present in the dataset:",ncol(Taxidata),"columns")

#Histogram of Trip distance in miles with outliers 

Histogram1 <- ggplot(data = Taxidata, aes(x = Trip_distance)) +
  geom_histogram(
    bins = 10,
    binwidth = 10,
    col = "black",
    fill = "blue",
    alpha = .2,
    position = "identity"
  ) +
  scale_x_continuous(name = 'Trip distance in miles',
                     breaks = seq(0, 600, 100),
                     expand = c(0, 0)) +
  scale_y_log10(expand = c(0, 0)) + labs(title = "Histogram with outliers") +
  theme_bw()

#Removing the ouliers using boxplot statistics

outlier <- boxplot.stats(Taxidata$Trip_distance)$out
variable <- ifelse(Taxidata$Trip_distance %in% outlier, NA, Taxidata$Trip_distance)

#Histogram of Trip distance in miles without outliers 

Histogram2 <- ggplot(Taxidata, aes(x = variable)) +
  geom_histogram(
    aes(y = ..density..),
    bins = 30,
    col = "black",
    fill = "#66CC99",
    alpha = .2,
    position = "identity"
  ) +
  scale_x_continuous(name = 'Trip distance in miles') + geom_density(alpha =.2,
                                                                     col = "red",
                                                                     linetype = "dashed") +
  theme_bw() + labs(title = "Histogram and Density plot without outliers")


#Used package "repr" to resize the plots in Jupyter notebook
options(repr.plot.width=8, repr.plot.height=3)

#Plotting the graphs using a grid
suppressWarnings(print(plot_grid(
  Histogram1, Histogram2, ncol = 2, nrow = 1
)))



#Printing Outlier Statistics
out1 <- sum(is.na(Taxidata$Trip_distance))
out2 <- sum(is.na(variable))

cat("Number of outliers identified:", out2 - out1, "rows","\n")
cat("Proportion (%) of outliers:", round((out2 - out1) / sum(!is.na(variable)) *
                                           100, 1), "%","\n")
cat("Mean with out removing the outliers:", round(mean(Taxidata$Trip_distance), 2),"\n")
cat("Mean after removing the outliers:", round(mean(variable, na.rm = TRUE), 2),"\n")


#Extracting columns lpep_pickup_datetime and Trip distance
TripDistanceByTime <- Taxidata[, c(2, 11)]
#Converting lpep_pickup_datetime in to date format
TripDistanceByTime[, 1] <- as.POSIXct(strptime(TripDistanceByTime[, 1], "%Y-%m-%d %H:%M:%S"))
#Extract hour from the lpep_pickup_datetime
TripDistanceByTime$hour <- hour(TripDistanceByTime[, 1])
TripDistanceByTime$lpep_pickup_datetime <- NULL

#Reshaping data
mlt <- melt(TripDistanceByTime, id = c("hour"))
res_mean <- dcast(mlt, hour ~ variable, mean)
res_median <- dcast(mlt, hour ~ variable, median)
Trip_mean_median <- cbind(res_mean, res_median$Trip_distance)
names(Trip_mean_median) <- c("Hour", "Mean_Trip_distance", "Median_Trip_distance")

cat("mean and median trip distance grouped by hour of day\n")
Trip_mean_median



#Pivoting the data using melt function
pmlt <-
  melt(
    Trip_mean_median,
    id.vars = "Hour",
    value.name = "value",
    variable.name = "Statistic"
  )

barplot <- ggplot(pmlt, aes(x = Hour, y = value, fill = Statistic)) + geom_bar(position = "dodge", stat = "identity") +
  theme(legend.position = "bottom") +
  labs(title = "Mean and Median trip distance by hour", y = "Distance")

options(repr.plot.width=8, repr.plot.height=3)
plot(barplot)



#The final rate code in effect at the end of the trip.
# 2=JFK
# 3=Newark

AirportData <- Taxidata %>%
  filter(
    RateCodeID %in% c(2, 3) &
      Pickup_longitude != 0 &
      Pickup_latitude != 0 &
      Dropoff_longitude != 0 & Dropoff_latitude != 0
  )

AirportData$RateCodeLocation <- AirportData[, 'RateCodeID']
AirportData$RateCodeLocation <- ifelse(AirportData$RateCodeLocation == 2, "JFK", "Newark")

cat("Number of trips originate or terminate at oneof the NYC area airports:", nrow(AirportData),"\n")
cat("Average fair of the trips originate or terminate at one of the NYC area airports:$",mean(AirportData$Fare_amount),"\n")
cat("Average tip amount given by the customers: $",mean(AirportData$Tip_amount),"\n")
cat("Average total amount of the trips originate or terminate at one of the NYC area airports:$",mean(AirportData$Total_amount),"\n")



factpal <-
  colorFactor(palette = c('red', 'yellow'),
              domain = AirportData$RateCodeLocation)

mp <-
  leaflet(AirportData) %>% addTiles('http://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png') %>% setView(-73.84427, 40.75517, zoom = 5)%>% addCircles(
  ~ Pickup_longitude,
  ~ Pickup_latitude,
  popup =  ~ RateCodeLocation,
  weight = 3,
  radius = 40,
  color = ~ factpal(RateCodeLocation),
  stroke = TRUE,
  fillOpacity = 0.8
) %>%
  addLegend(
    "bottomright",
    colors = c("red", "yellow"),
    labels = c("JFK", "Newark"),
    title = "Rate code"
  ) %>%
  addMarkers(-73.7781, 40.6413, popup = "JFK") %>%
  addMarkers(-74.1745, 40.6895, popup = "Newark")



tf = 'myfigure.html'
htmlwidgets::saveWidget(mp, file = tf, selfcontained = F)
IRdisplay::display_html(paste("<iframe src=' ", tf, " ' width=900 height=400","/>"))

# Distribution of payments

AirportData$Payment_type[AirportData$Payment_type == 1] <- "Credit Card"
AirportData$Payment_type[AirportData$Payment_type == 2] <- "Cash"
AirportData$Payment_type[AirportData$Payment_type == 3] <- "No Charge"
AirportData$Payment_type[AirportData$Payment_type == 4] <- "Dispute"
AirportData$Payment_type[AirportData$Payment_type == 5] <- "Unknown"
AirportData$Payment_type[AirportData$Payment_type == 6] <- "Voided Trip"

type <- data.frame(table(AirportData$Payment_type))

options(repr.plot.width=8, repr.plot.height=5)
ggplot(type, aes(
  x = Var1,
  y = Freq,
  fill = Var1,
  label = Var1
)) +
  geom_bar(stat = "identity") +
  geom_text(aes(label = Freq), vjust = -0.5) +
  theme_bw() +
  labs(title = "Distribution of payments") + labs(x = "Payment Type", y =
                                                    "Count") +
  scale_fill_discrete(name = "Payment Type")

#Hourly trip distributions Airport vs Non-Airport.

TripDistributions <- Taxidata[, c("lpep_pickup_datetime", 'Trip_distance', 'RateCodeID')]
TripDistributions[, 1] <- as.POSIXct(strptime(TripDistributions[, 1], "%Y-%m-%d %H:%M:%S"))
TripDistributions$hour <- hour(TripDistributions[, 1])

TripDistributions$Trip_Type <-
  ifelse(
    TripDistributions$RateCodeID == 2 |
      TripDistributions$RateCodeID == 3,
    "Airport trip",
    "Non-Airport trip"
  )

Trips <- sqldf('select hour,Trip_Type,count(Trip_Type) as TripNum from TripDistributions group by hour,Trip_Type order by Trip_type ')

AirportTrips <- Trips[1:24, ]
NonAirportTrips <- Trips[25:48, ]

AirTrips <- data.frame(cbind(AirportTrips$hour,AirportTrips$TripNum, NonAirportTrips$TripNum))
names(AirTrips) <- c('Hours', 'AirportTrips', 'Non-AirportTrips')

#standardize the range of independent variables between Airport and Non-Airport trips
AirTrips$AirportTrips <- scale(AirTrips$AirportTrips, center = FALSE, scale = TRUE)
AirTrips$`Non-AirportTrips` <- scale(AirTrips$`Non-AirportTrips`,center = FALSE,scale = TRUE)

#pivoting the data
AirTripsdata <- melt(AirTrips, id.vars = 'Hours')

options(repr.plot.width=8, repr.plot.height=5)

#Line plot to identify the distribution of Airports vs Non-Airport trips
ggplot(data = AirTripsdata, aes(
  x = Hours,
  y = value,
  group = variable,
  colour = variable
)) +
  geom_line() + geom_point(size = 2, shape = 1) +
  theme_bw() +
  labs(title = "Distribution Airport and Non-Airport trips") +
  labs(x = "Hours", y = "Scaled trip count") +
  scale_fill_discrete(name = "Trip type")


Taxidata$Total_amount <- abs(Taxidata$Total_amount)
Taxidata$Tip_amount <- abs(Taxidata$Tip_amount)
Taxidata$Total_amount <- ifelse(Taxidata$Total_amount < 2.5,median(Taxidata$Total_amount),Taxidata$Total_amount)
Taxidata$Tip_percent <- (Taxidata$Tip_amount/Taxidata$Total_amount)* 100

cat('Summary statistics of tip percentage')
summary(Taxidata$Tip_percent)

cat('Preview of dataset with tip percentage')
head(Taxidata)

#Data Cleaning
Taxidata$lpep_pickup_datetime <- as.POSIXct(strptime(Taxidata[, 'lpep_pickup_datetime'], "%Y-%m-%d %H:%M:%S"))
Taxidata$Lpep_dropoff_datetime <- as.POSIXct(strptime(Taxidata[, 'Lpep_dropoff_datetime'], "%Y-%m-%d %H:%M:%S"))
Taxidata$RateCodeID <- ifelse(Taxidata$RateCodeID==99,1,Taxidata$RateCodeID)
Taxidata$Fare_amount <- abs(Taxidata$Fare_amount)

Taxidata$Extra <- abs(Taxidata$Extra)
Taxidata$Extra <- ifelse(Taxidata$Extra==0.5|Taxidata$Extra==1,Taxidata$Extra,0)

Taxidata$MTA_tax <- abs(Taxidata$MTA_tax)
Taxidata$Tip_amount <- abs(Taxidata$Tip_amount)
Taxidata$Tolls_amount <- abs(Taxidata$Tolls_amount)
Taxidata$improvement_surcharge <- abs(Taxidata$improvement_surcharge)
Taxidata$Total_amount <- abs(Taxidata$Total_amount)
Taxidata$Total_amount <- ifelse(Taxidata$Total_amount < 2.5,median(Taxidata$Total_amount),Taxidata$Total_amount)
Taxidata$Ehail_fee <- NULL
Taxidata$Trip_type[is.na(Taxidata$Trip_type)]<- 1
Taxidata$Trip_distance <- ifelse(Taxidata$Trip_distance>0,Taxidata$Trip_distance,1.980)

#Feature Engineering
#Date variables
#Trip duration
#Speed calculation

#computing date variables
Taxidata$Trip_week_day <- wday(Taxidata$lpep_pickup_datetime)
Taxidata$Trip_weekNumber <- isoweek(Taxidata$lpep_pickup_datetime)
Taxidata$Trip_dayNumber <- day(Taxidata$lpep_pickup_datetime)
Taxidata$Trip_hour <- hour(Taxidata$lpep_pickup_datetime)

#computing trip duration
Taxidata$Trip_duration <- as.numeric(difftime(Taxidata$Lpep_dropoff_datetime,Taxidata$lpep_pickup_datetime,units = "mins"))
Taxidata$Trip_duration <- ifelse(Taxidata$Trip_duration==0,median(Taxidata$Trip_duration),Taxidata$Trip_duration)

#Speed calculation
Taxidata$Trip_speed <- (Taxidata$Trip_distance)/(Taxidata$Trip_duration/60)

#Outlier removal function
remove_outliers <- function(x, na.rm = TRUE, ...) {
  qnt <- quantile(x, probs=c(.25, .75), na.rm = na.rm, ...)
  H <- 1.5 * IQR(x, na.rm = na.rm)
  y <- x
  y[x < (qnt[1] - H)] <- NA
  y[x > (qnt[2] + H)] <- NA
  y
}

Taxidata$Trip_speed <- remove_outliers(Taxidata$Trip_speed)

#Random data generator with same distribution of data
random.imp <- function (a){
  missing <- is.na(a)
  n.missing <- sum(missing)
  a.obs <- a[!missing]
  imputed <- a
  imputed[missing] <- sample (a.obs, n.missing, replace=TRUE)
  return (imputed)
}


Taxidata$Trip_speed <- random.imp(Taxidata$Trip_speed)

summary(Taxidata$Trip_speed)

#Print the dataset dimenstions after feature engineering
cat("Number of rows after feature engineering:",nrow(Taxidata),"rows\n")
cat("Number of columns after feature engineering:",ncol(Taxidata),"columns")

Histogram3 <- ggplot(data = Taxidata, aes(Tip_amount)) +
  geom_histogram(
    bins = 30,
    binwidth = 10,
    position = "identity",
    col = 'black',
    fill = "green",
    alpha = .2
  ) +
  scale_x_continuous(breaks = seq(0, 300, 50),
                     name = 'Tip amount in dollars',
                     expand = c(0, 0)) + scale_y_log10(expand = c(0, 0))+
  labs(title = "Histogram of tip amount ")

Scatter1 <- ggplot(data=Taxidata,aes(x=Fare_amount,y=Tip_percent))+geom_point(stat = 'identity',inherit.aes = TRUE,col = 'blue')+
  scale_y_continuous(breaks = seq(0, 100, 10),
                     name = 'Tip Percentage',
                     expand = c(0, 0))+
  scale_x_continuous(breaks =seq(0, 580, 100),name='Fare amount')+
  labs(title = "Scatterplot Tip percentage vs Fare amount")


scatter2 <- ggplot(Taxidata,aes(Trip_duration,Tip_percent))+geom_point(stat = 'identity',inherit.aes = TRUE,col = 'red')+
  scale_y_continuous(breaks = seq(0, 100, 10),
                     name = 'Tip Percentage',
                     expand = c(0, 0))+
  scale_x_continuous(breaks =seq(0, 1500, 100),name='Trip duration')+
  labs(title = "Scatterplot Tip percentage vs Trip duration")

options(repr.plot.width=6, repr.plot.height=12)

suppressWarnings(print(plot_grid(
  Histogram3, Scatter1,scatter2, ncol = 1, nrow = 3
)))


#Correlation between continues variables

vars <- c('Total_amount','Fare_amount','Trip_distance','Trip_duration','Tolls_amount','Trip_speed','Tip_percent')
numericdata <- Taxidata[,vars]
corr <- cor(numericdata[,1:ncol(numericdata)-1])

options(repr.plot.width=8, repr.plot.height=5)
corrplot(corr,type="lower",order = "hclust",tl.col = "black")

Taxidata$Tipped <- ifelse(Taxidata$Tip_amount>0,1,0)
tab <- table(Taxidata$Payment_type,Taxidata$Tipped)
colnames(tab) <- c("Not Tipped","Tipped")
row.names(tab) <- c("Credit card","Cash","No Charge","Dispute","Unknown")
cat('PaymentType vs Tip')
tab

trainData <- Taxidata
Vars <- c("Extra","MTA_tax","Total_amount","Payment_type",
                    "Trip_duration","Trip_hour","Trip_week_day","Trip_speed","Tipped")
data <- trainData[,Vars]
data$Tipped <- as.factor(data$Tipped)
levels(data$Tipped) <- make.names(levels(factor(data$Tipped)))

predictors <- c("Extra","MTA_tax","Total_amount","Payment_type",
                        "Trip_duration","Trip_hour","Trip_week_day","Trip_speed")

outcome <- c('Tipped')

set.seed(128)

trainIndex <- createDataPartition(data$Tipped, p =.1,list =FALSE)
training <- data[trainIndex,]
testing <- data[-trainIndex,]

#basic parameter tuning
objControl <- trainControl(method='cv', 
                           number=5, 
                           returnResamp='none', 
                           summaryFunction = twoClassSummary, 
                           classProbs = TRUE)

gbmGrid <- expand.grid(interaction.depth = (1:5) * 2,
                     n.trees = seq(1,150,5),
                     n.minobsinnode =2,
                    shrinkage = .1)

#fitControl

fit.gbm1 <- suppressWarnings(train(training[,predictors] ,training[,outcome],
                method='gbm',
                metric="Accuracy",
                preProc = c("center","scale"),
                trControl=objControl,
                tuneGrid = gbmGrid
                ))

Rocplot <- ggplot(fit.gbm1) 
varImp <- ggplot(varImp(fit.gbm1))

cat('ROC Curve')
options(repr.plot.width=8, repr.plot.height=4)
suppressWarnings(Rocplot)

cat('Variable Importance')
options(repr.plot.width=8, repr.plot.height=4)
suppressWarnings(varImp)


# make predictions
xgbmtest <- testing[,predictors]
ygbmtest <- testing[,outcome]
predictions <- predict(fit.gbm1, xgbmtest)
# summarize results
cat('Accuracy and Kappa scores on testing data')
postResample(pred = predictions,obs = ygbmtest )

Reg.vars <- c('VendorID', 'Passenger_count', 'Trip_distance', 'Total_amount', 
                                'Extra', 'MTA_tax', 'Tolls_amount','Trip_hour','Trip_week_day', 'Trip_weekNumber', 'Trip_dayNumber',  
                                'Trip_duration', 'Trip_speed','Tip_percent')

Reg.predictors <- c('VendorID', 'Passenger_count', 'Trip_distance', 'Total_amount', 
                'Extra', 'MTA_tax', 'Tolls_amount','Trip_hour','Trip_week_day', 'Trip_weekNumber', 'Trip_dayNumber',  
                'Trip_duration', 'Trip_speed')

Reg.outcome <- c('Tip_percent')
reg.datasub <- subset(Taxidata,Tip_percent>0)

set.seed(230)
reg.trainIndex <- createDataPartition(reg.datasub$Tip_percent, p =.1,list =FALSE)
reg.training <- reg.datasub[reg.trainIndex,]
reg.testing <- reg.datasub[-reg.trainIndex,]

reg.trainControl <- trainControl(method="cv", number=3)

reg.gbmGrid <- expand.grid(interaction.depth = 12,
                           n.trees = 150,
                           n.minobsinnode = 13,
                           shrinkage = 0.1)

reg.fit.gbm1 <- train(Tip_percent~., data=reg.training[,Reg.vars], method="gbm", metric="RMSE",
                      trControl=reg.trainControl,tuneGrid = reg.gbmGrid)


reg.predict.gbm <- data.frame(predict(object = reg.fit.gbm1,newdata = reg.testing[,Reg.vars]))
colnames(reg.predict.gbm) <- c('Predicted Tip Percentage')
actualPredicted <- data.frame(cbind(reg.testing$Tip_percent,reg.predict.gbm))
colnames(actualPredicted) <- c('Actual Tip percentage','Predicted Tip percentage')

RMSError <- rmse(actualPredicted$`Actual Tip percentage`,actualPredicted$`Predicted Tip percentage`)
MAError <- mae(actualPredicted$`Actual Tip percentage`,actualPredicted$`Predicted Tip percentage`)
cat('RMSE Error on testing data',RMSError,'\n')
cat('Mean Absolute Error on testing data',MAError,'\n')

actualPredicted$Residuals <- (actualPredicted$`Actual Tip percentage`)-(actualPredicted$`Predicted Tip percentage`)

options(repr.plot.width=8, repr.plot.height=4)

ggplot(actualPredicted, aes(x = Residuals)) +
  geom_histogram(
    aes(y = ..density..),
    bins = 120,
    col = "black",
    fill = "#66CC99",
    alpha = .5,
    position = "identity"
  ) +
  scale_x_continuous(name = 'Residuals') + geom_density(alpha =.2,
                                                        col = "red",
                                                        linetype = "dashed") +
  theme_bw() + labs(title = "Histogram of Residuals (Acutal-Predicted)")



summary(Taxidata$Trip_speed)

AvgSpeed <- sqldf("select Trip_weekNumber,avg(Trip_speed) as AvegrageSpeed from Taxidata group by Trip_weekNumber")

cat('Average speed over the week')
AvgSpeed

options(repr.plot.width=8, repr.plot.height=4)
ggplot(Taxidata, aes(x=Trip_weekNumber, y=Trip_speed,group=Trip_weekNumber,fill=as.factor(Trip_weekNumber))) + 
  geom_boxplot()+
  labs(title="Box plot - Average speed over the weeks",x="Week Number", y = " Average speed")+scale_fill_discrete(name = "Trip Week")


cat('Pairwise t test:')
pairwise.t.test(Taxidata$Trip_speed,Taxidata$Trip_weekNumber)

sqldf("select Trip_hour,avg(Trip_speed) as AvegrageSpeed from Taxidata group by Trip_hour order by Trip_hour")

HourAvgSpeed.anova <- aov(Taxidata$Trip_speed~Taxidata$Trip_hour)
summary(HourAvgSpeed.anova)

Hourly.pairwise <- pairwise.t.test(Taxidata$Trip_speed,Taxidata$Trip_hour)
Hourly.pairwise

options(repr.plot.width=8, repr.plot.height=4)
ggplot(Taxidata, aes(x=as.factor(Trip_hour), y=Trip_speed,group=as.factor(Trip_hour),fill=as.factor(Trip_hour))) + 
  geom_boxplot()+
  labs(title="Box plot - Average speed over the hours",x="Hours", y = " Average speed")+scale_fill_discrete(name = "Trip hour")


stopCluster(cl)
